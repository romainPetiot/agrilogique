<?php

/*
 * ACF BLOCKS
 * Source: https://www.advancedcustomfields.com/blog/acf-5-8-introducing-acf-blocks-for-gutenberg/
 * ----------------------------------------------------------------------------*/

function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'agri',
                'title' => __( 'Agrilogique' ),
                'icon'  => 'carrot'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );

add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block') ) {
    
        acf_register_block(
            array(
                'name'				    => 'slide-video',
                'title'				    => __('Slide Vidéo'),
                'description'		    => __('Slide Vidéo'),
                'placeholder'		    => __('Slide Vidéo'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('Slide Vidéo', 'slide', 'video', 'accueil', 'home')

            )
        );

        acf_register_block(
            array(
                'name'				    => 'slide-main-home',
                'title'				    => __('Accueil : Slide principale'),
                'description'		    => __('Accueil : Slide principale'),
                'placeholder'		    => __('Accueil : Slide principale'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('Accueil : Slide principale', 'principale', 'démarche', 'nouvelle', 'home')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'slide-second-home',
                'title'				    => __('Accueil : Slide secondaire'),
                'description'		    => __('Accueil : Slide secondaire'),
                'placeholder'		    => __('Accueil : Slide secondaire'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('Accueil', 'Slide', 'secondaire', 'home')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'proof-header',
                'title'				    => __('Preuves entête'),
                'description'		    => __('Preuves entête'),
                'placeholder'		    => __('Preuves entête'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('preuves', '5')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'proof-slide',
                'title'				    => __('Slide preuve'),
                'description'		    => __('Slide preuve'),
                'placeholder'		    => __('Slide preuve'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('preuve', 'slide')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'editor',
                'title'				    => __('Editeur fond sombre'),
                'description'		    => __('Editeur fond sombre'),
                'placeholder'		    => __('Editeur fond sombre'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('editeur', 'fond', 'sombre', 'texte', 'wysiwyg')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'slider',
                'title'				    => __('Carrousel Agriculteur'),
                'description'		    => __('Carrousel Agriculteur'),
                'placeholder'		    => __('Carrousel Agriculteur'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('Carrousel', 'Agriculteur', 'slider', 'slideshow')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'map-farmer',
                'title'				    => __('Carte Agriculteurs'),
                'description'		    => __('Carte Agriculteurs'),
                'placeholder'		    => __('Carte Agriculteurs'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('carte', 'Agriculteur', 'map')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'faq',
                'title'				    => __('Foire aux questions (FAQ)'),
                'description'		    => __('Foire aux questions (FAQ)'),
                'placeholder'		    => __('Foire aux questions (FAQ)'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('faq', 'Foire', 'questions')
            )
        );

        acf_register_block(
            array(
                'name'				    => 'Title',
                'title'				    => __('Titre h1'),
                'description'		    => __('Titre h1'),
                'placeholder'		    => __('Titre h1'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'agri',
                //'icon'				    => 'playlist-video',
                'mode'                  => 'edit',
                'keywords'			    => array('Titre', 'h1',)
            )
        );
    }
}


function getRenderTemplateHTML($block) {
 // Rendu du bloc
 $slug = str_replace('acf/', '', $block['name']);

 // include a template part from within the "template-parts/block" folder
 if ( file_exists(get_theme_file_path("/template-parts/block/{$slug}.php")) ) :
   include(get_theme_file_path("/template-parts/block/{$slug}.php") );
 endif;
}