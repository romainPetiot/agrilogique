var accordions = document.getElementsByClassName("accordion");
 
for (var i = 0; i < accordions.length; i++) {
  accordions[i].onclick = function() {
    this.classList.toggle('is-open');
 
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      // accordion is currently open, so close it
      content.style.maxHeight = null;
    } else {
      // accordion is currently closed, so open it
      content.style.maxHeight = content.scrollHeight + "px";
    }
  };
}
// Get IE or Edge browser version
//document.addEventListener("DOMContentLoaded", function(event) { 
var version = detectIE();
//});
/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    document.body.className += " ie";
    return true;
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    document.body.className += " ie";
    return true;
  }

  return false;
}
document.addEventListener("DOMContentLoaded", function(event) { 
    document.getElementById("link-menu").addEventListener("click", function(e) { 
        e.preventDefault();
        document.getElementById("cont-menu-full-screen").classList.toggle("openMenu");
    });
    document.getElementById("link-menu-ie").addEventListener("click", function(e) { 
        e.preventDefault();
        document.getElementById("cont-menu-full-screen-ie").classList.toggle("openMenu");
    });
    document.getElementById("cont-menu-full-screen").addEventListener("click", function(e) {
        document.getElementById("cont-menu-full-screen").classList.remove("openMenu");
    });
    document.getElementById("cont-menu-full-screen-ie").addEventListener("click", function(e) {
        document.getElementById("cont-menu-full-screen-ie").classList.remove("openMenu");
    });
}); 
 

document.addEventListener("DOMContentLoaded", function(event) {
    var els = document.getElementsByClassName("proof-slide");
    Array.prototype.forEach.call(els, function(el) {
        var menu = el.getElementsByClassName("proof");
        if(menu[0]){
            Array.prototype.forEach.call(menu[0].getElementsByClassName("menu-item"), function(li) {
                if(li.getElementsByTagName("a")[0].getAttribute("href") == "#"+el.id ){
                    li.classList.add("active");                    
                }
            });
        }
    });
});


document.addEventListener("DOMContentLoaded", function(event) {
    var els = document.getElementsByClassName("slide-second-home");
    Array.prototype.forEach.call(els, function(el) {
        var menu = el.getElementsByClassName("home");
        if(menu[0]){
            Array.prototype.forEach.call(menu[0].getElementsByClassName("menu-item"), function(li) {
                if(li.getElementsByTagName("a")[0].getAttribute("href") == "#"+el.id ){
                    li.classList.add("active");                    
                }
            });
        }
    });
});

document.addEventListener("DOMContentLoaded", function(event) {

    var els = document.getElementsByClassName("video");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            modalID = document.getElementById(el.dataset.id);
            modalInside = document.createElement("div");
            modalInside.classList.add("modalInside");

            video = document.createElement("div");
            video.classList.add("video-container");
            modalInside.append(video);
            video.innerHTML = el.dataset.video;

            modalID.append(modalInside);

            var x = document.createElement("div");
            x.classList.add("closeModal");
            x.classList.add("close");
            x.append("X");
            modalID.append(x);

            modalID.classList.add("active");
            modalID.classList.add("closeModal");

            x.addEventListener("click", function(e) {
                modalID.classList.remove("active");
                modalInside.remove();
            });

            modalID.addEventListener("click", function(e) {
                modalID.classList.remove("active");
                modalInside.remove();
            });

        });
    });
});


var els = document.getElementsByClassName("menu-lang");
Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
        e.preventDefault();
        modal = document.getElementById("modal-lang");
        modal.classList.add("active");

        modal.addEventListener("click", function(e) {
            modal.classList.remove("active");
        });
    });
});




document.addEventListener("DOMContentLoaded", function(event) {

    //ouverture de la modal

    //fermeture de la modal 
    var closeModals = document.getElementsByClassName("closeModal");
    Array.prototype.forEach.call(closeModals, function(closeModal) {
        closeModal.addEventListener("click", function(e) {
            e.preventDefault();

            var modals = document.getElementsByClassName("modal");
            Array.prototype.forEach.call(modals, function(modal) {
                modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });
    });

});

document.addEventListener("DOMContentLoaded", function(event) { 
    if(document.getElementById("moreRecipes")) {
        document.getElementById("moreRecipes").addEventListener("click", function(e) { 
            e.preventDefault();

            var recipe = document.getElementsByClassName("recipe"); 
            for (var i = 0; i < recipe.length; i++) { 
                recipe[i].classList.add("recipeActive");
            }
        });
    }
});

var slideIndex = 1;


// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {slideIndex = 1;}
    if (n < 1) {slideIndex = slides.length;}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    if(slides[slideIndex-1]){
        slides[slideIndex-1].style.display = "flex";
    }
}

        
document.addEventListener("DOMContentLoaded", function(event) { 

    showSlides(slideIndex);

    if(document.getElementsByClassName("mySlides").length > 0 && !detectIE()) { 

        var els = document.getElementsByClassName("bloc-img");
        Array.prototype.forEach.call(els, function(el) {
            el.addEventListener("click", function(e) { 
                e.preventDefault();
                modalID = document.getElementById(el.dataset.id);
                
                modalID.innerHTML = '<img src="'+ el.dataset.image +'"><div>'+el.dataset.content+'</div>';
                modalID.classList.toggle("active");
                document.getElementById("BGmodal").classList.toggle("active");
            });
        });

    }
});
