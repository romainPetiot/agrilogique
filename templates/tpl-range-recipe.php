<?php
/**
 * Template Name: Gamme et listing recettes
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<?php the_content();?>

<div class="fullGrid">
<?php
$terms = get_terms( array(
    'taxonomy' => 'range',
) );
?>
<?php foreach ( $terms as $term ) :?>
    <?php 
    $image = get_field('bg-image', $term);
    $bg = '';
    $size = 'bg-image';
    if( $image ) :
        if($bg = wp_get_attachment_image_src( $image, $size )){
            $bg = $bg[0];
        };
    endif;
    ?>
    <div class="recipe-content" style="background-image:url(<?php echo $bg;?>);">
        <?php echo '<h2>'.$term->name . '</h2>';?>
        <?php 
        $image = get_field('image', $term);
        $size = 'gamme';
        if( $image ) :
            echo wp_get_attachment_image( $image, $size );
        endif;
        ?>

        <?php if(!empty(get_field("link", $term)) && $label = get_field("link-label", $term)):?>
            <a href="<?php the_field("link", $term);?>" target="_blank" class="button"> 
                <?php echo $label;?>
            </a>
        <?php endif;?>

    </div>
<?php endforeach;?>
</div>

<div id="recipesArchive"> <!-- je n'ai pas d'info sur l'image en bg -->
    <h2><?php _e("Vous voulez des idées pour les cuisiner ?", "agrilogique");?></h2>    
    <div class="wrapper fullGrid">         
            <?php
            global $post;
            $args = array( 
                    'posts_per_page'   => -1,
                    'post_type'        => 'recipe',
                    'post_status'      => 'publish',);
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : 
            setup_postdata( $post ); ?>
                <div class="recipe grid33">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail("recipe-archive");?>
                        <h3><?php the_title(); ?></h3>
                    </a>
                </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
    </div>
    <button id="moreRecipes" class="button"><?php _e("Voir + de recettes", "agrilogique");?></button>
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
