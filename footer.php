

	</div><!-- div #content -->
	
	<?php 
	$image = get_field('bg-image-footer', 'option');
	$bg = '';
	$size = 'bg-image-footer';
	if( $image ) :
		if($bg = wp_get_attachment_image_src( $image, $size )){
			$bg = $bg[0];
		};
	endif;
	?>
	<footer id="footer" style="background-image:url(<?php echo $bg;?>);">
		<div class="footer-content">
			<div>
				<?php
					echo ihag_menu('footer');
				?>
				<span><?php the_field('txt-footer','option');?></span>
			</div>
			<div>
			<?php
				if( have_rows('images-footer','option') ):
					while ( have_rows('images-footer','option') ) : the_row();
						$image = get_sub_field('image');
						$size = 'small-logo';
						if( $image ) :
							?>
								<a class="logo-footer" href="<?php the_sub_field('link')?>" target="_blank" title="<?php _e("lien vers ", "agrilogique"); echo get_post_meta($image, '_wp_attachment_image_alt', true); _e(" - ouverture dans un autre onglet", "agrilogique");?>">
									<?php
										echo wp_get_attachment_image( $image, $size );
									?>
								</a>
							<?php
						endif;
					endwhile;
				endif;
			?>
			</div>
		</div>
	</footer>
</div><!-- div #page -->

<?php wp_footer(); ?>

</body>
</html>

