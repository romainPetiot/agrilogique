// Get IE or Edge browser version
//document.addEventListener("DOMContentLoaded", function(event) { 
var version = detectIE();
//});
/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    document.body.className += " ie";
    return true;
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    document.body.className += " ie";
    return true;
  }

  return false;
}