document.addEventListener("DOMContentLoaded", function(event) {

    var els = document.getElementsByClassName("video");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            modalID = document.getElementById(el.dataset.id);
            modalInside = document.createElement("div");
            modalInside.classList.add("modalInside");

            video = document.createElement("div");
            video.classList.add("video-container");
            modalInside.append(video);
            video.innerHTML = el.dataset.video;

            modalID.append(modalInside);

            var x = document.createElement("div");
            x.classList.add("closeModal");
            x.classList.add("close");
            x.append("X");
            modalID.append(x);

            modalID.classList.add("active");
            modalID.classList.add("closeModal");

            x.addEventListener("click", function(e) {
                modalID.classList.remove("active");
                modalInside.remove();
            });

            modalID.addEventListener("click", function(e) {
                modalID.classList.remove("active");
                modalInside.remove();
            });

        });
    });
});


var els = document.getElementsByClassName("menu-lang");
Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
        e.preventDefault();
        modal = document.getElementById("modal-lang");
        modal.classList.add("active");

        modal.addEventListener("click", function(e) {
            modal.classList.remove("active");
        });
    });
});




document.addEventListener("DOMContentLoaded", function(event) {

    //ouverture de la modal

    //fermeture de la modal 
    var closeModals = document.getElementsByClassName("closeModal");
    Array.prototype.forEach.call(closeModals, function(closeModal) {
        closeModal.addEventListener("click", function(e) {
            e.preventDefault();

            var modals = document.getElementsByClassName("modal");
            Array.prototype.forEach.call(modals, function(modal) {
                modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });
    });

});