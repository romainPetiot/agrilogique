
document.addEventListener("DOMContentLoaded", function(event) { 
    if(document.getElementById("moreRecipes")) {
        document.getElementById("moreRecipes").addEventListener("click", function(e) { 
            e.preventDefault();

            var recipe = document.getElementsByClassName("recipe"); 
            for (var i = 0; i < recipe.length; i++) { 
                recipe[i].classList.add("recipeActive");
            }
        });
    }
});