document.addEventListener("DOMContentLoaded", function(event) { 
    document.getElementById("link-menu").addEventListener("click", function(e) { 
        e.preventDefault();
        document.getElementById("cont-menu-full-screen").classList.toggle("openMenu");
    });
    document.getElementById("link-menu-ie").addEventListener("click", function(e) { 
        e.preventDefault();
        document.getElementById("cont-menu-full-screen-ie").classList.toggle("openMenu");
    });
    document.getElementById("cont-menu-full-screen").addEventListener("click", function(e) {
        document.getElementById("cont-menu-full-screen").classList.remove("openMenu");
    });
    document.getElementById("cont-menu-full-screen-ie").addEventListener("click", function(e) {
        document.getElementById("cont-menu-full-screen-ie").classList.remove("openMenu");
    });
}); 
 

document.addEventListener("DOMContentLoaded", function(event) {
    var els = document.getElementsByClassName("proof-slide");
    Array.prototype.forEach.call(els, function(el) {
        var menu = el.getElementsByClassName("proof");
        if(menu[0]){
            Array.prototype.forEach.call(menu[0].getElementsByClassName("menu-item"), function(li) {
                if(li.getElementsByTagName("a")[0].getAttribute("href") == "#"+el.id ){
                    li.classList.add("active");                    
                }
            });
        }
    });
});


document.addEventListener("DOMContentLoaded", function(event) {
    var els = document.getElementsByClassName("slide-second-home");
    Array.prototype.forEach.call(els, function(el) {
        var menu = el.getElementsByClassName("home");
        if(menu[0]){
            Array.prototype.forEach.call(menu[0].getElementsByClassName("menu-item"), function(li) {
                if(li.getElementsByTagName("a")[0].getAttribute("href") == "#"+el.id ){
                    li.classList.add("active");                    
                }
            });
        }
    });
});
