
var slideIndex = 1;


// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {slideIndex = 1;}
    if (n < 1) {slideIndex = slides.length;}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    if(slides[slideIndex-1]){
        slides[slideIndex-1].style.display = "flex";
    }
}

        
document.addEventListener("DOMContentLoaded", function(event) { 

    showSlides(slideIndex);

    if(document.getElementsByClassName("mySlides").length > 0 && !detectIE()) { 

        var els = document.getElementsByClassName("bloc-img");
        Array.prototype.forEach.call(els, function(el) {
            el.addEventListener("click", function(e) { 
                e.preventDefault();
                modalID = document.getElementById(el.dataset.id);
                
                modalID.innerHTML = '<img src="'+ el.dataset.image +'"><div>'+el.dataset.content+'</div>';
                modalID.classList.toggle("active");
                document.getElementById("BGmodal").classList.toggle("active");
            });
        });

    }
});