<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" sizes="32x32">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="modal" id="modal-lang">
	<div id="modal-lang-content">
		<div class="close closeModal">X</div>
		<div class="modal-lang-content-header">
			<b><?php _e("Sélectionnez votre pays et langage");?></b>
		</div>
		<div class="modal-menu-lang">
			<?php 
				$langs = pll_the_languages(array( 'raw' => 1 ));
				foreach($langs as $lang):?>
					<div>
						<a href="<?php echo $lang['url'];?>">
							<?php echo ($lang['locale'] == 'fr-FR')?'France':'Deutschland';?>
						</a>
						<br>
						<a href="<?php echo $lang['url'];?>">
							<?php echo $lang['name'];?>
						</a>
					</div>
				<?php endforeach;		
			?>
		</div>
	</div>
</div>
<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Aller au contenu', 'agrilogique' ); ?></a>
	<a class="skip-link screen-reader-text" href="#menu"><?php esc_html_e( 'Aller au menu', 'agrilogique' ); ?></a>
	<header id="masthead" class="headerIE">
		<div class="prehead wrapper">
			<a class="custom_logo" href="<?php echo home_url();?>">
			<?php
			//logo champs ACF la taille "logo" est gérée depuis le fichier inc/image.php
			$image = get_field('custom_logo','option');
			$size = 'logo';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}
			?>
			</a>
			<span><?php the_field('txt-header','option'); ?>	</span>
			<?php
				if( have_rows('images-header','option') ):
					while ( have_rows('images-header','option') ) : the_row();
						$image = get_sub_field('image');
						$size = 'small-logo';
						if( $image ) :
							?>
							<?php
								echo wp_get_attachment_image( $image, $size );
							?>
							<?php
						endif;
					endwhile;
				endif;
			?>

		</div>
		<div class="menu-bg">	
			<div class="wrapper">
				<nav role="navigation" aria-label="Menu secondaire" id="menu-desktop">
					<!-- pour faciliter l'intégration il y deux menus en version desktop -->
					<div id="menu-main-ie">
						<?php
							echo ihag_menu('primary-ie');
						?>
					</div>
				</nav>
			</div>
			<div id="cont-menu">
				<button id="link-menu-ie" aria-label="ouvrir le menu" aria-expanded="false"> <span class="burger"></span> <span class="cross"></span> </button>
			</div>
		</div>

		<div id="cont-menu-full-screen-ie">
			<nav role="navigation" aria-label="Menu principal" id="menu-ie"><!-- Le menu mobile est le menu principal pour l'acessibilité (car il est complet)-->
			<?php
				echo ihag_menu('primary-mobile');
			?>
			</nav>
		</div>
	</header>

	<header id="masthead" class="headerNotIE">
		<div class="prehead">
		<span><?php the_field('txt-header','option'); ?>	</span>
		<?php
			if( have_rows('images-header','option') ):
				while ( have_rows('images-header','option') ) : the_row();
					$image = get_sub_field('image');
					$size = 'small-logo';
					if( $image ) :
						?>
						<?php
							echo wp_get_attachment_image( $image, $size );
						?>
						<?php
					endif;
		   		endwhile;
			endif;
		?>

		</div>
		<div class="menu-bg">	
			<div class="wrapper">
				<nav role="navigation" aria-label="Menu secondaire" id="menu-desktop">
					<!-- pour faciliter l'intégration il y deux menus en version desktop -->
					<div id="menu-left">
						<?php
							echo ihag_menu('primary-left');
						?>
					</div>
					<div>
						<a class="custom_logo" href="<?php echo home_url();?>">
						<?php
						//logo champs ACF la taille "logo" est gérée depuis le fichier inc/image.php
						$image = get_field('custom_logo','option');
						$size = 'logo';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}
						?>
						</a>
					</div>
					<div id="menu-right">
						<?php
							echo ihag_menu('primary-right');
						?>
					</div>
				</nav>
			</div>
			<div id="cont-menu">
				<button id="link-menu" aria-label="ouvrir le menu" aria-expanded="false"> <span class="burger"></span> <span class="cross"></span> </button>
			</div>
		</div>

		<div id="cont-menu-full-screen">
			<nav role="navigation" aria-label="Menu principal" id="menu"><!-- Le menu mobile est le menu principal pour l'acessibilité (car il est complet)-->
			<?php
				echo ihag_menu('primary-mobile');
			?>
			</nav>
		</div>
	</header>


	<?php 
	$desktop = '';
	if(has_post_thumbnail() && !is_singular('recipe') )
	{
    	$desktop_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'desktop');
		$desktop = 'style="background-image: url('.$desktop_src[0].');"'; ?>
		<div id="content" <?php echo $desktop; ?>>
	<?php } elseif(is_singular('recipe')) { ?>
		<div id="content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/image/bg-recette.png');">
	<?php } else { ?>
		<div id="content">
	<?php } ?> 
	
		<a class="ancre-scrollto" href="#page"><span><?php _e("Haut<br>de page","agrilogique");?></span></a>
			
		<?php if(!is_front_page()):
			wpBreadcrumb();
		endif;?>
