<div class="wrapper">
    <div class="fullGrid">    
        <div class="grid40">
            <?php the_post_thumbnail("recipe");?>
        </div>
        <div class="grid60">
            <h1><?php the_title();?></h1>
            <h2><?php _e("Ingrédients", "agrilogique");?></h2>
            <ul>
            <?php
            if( have_rows('ingredients') ):
                while ( have_rows('ingredients') ) : the_row();
                    echo '<li>';
                    the_sub_field('ingredient');
                    echo '</li>';
                endwhile;
            endif;
            ?>
            </ul>
        </div>

        <div class="grid100 preparation">
            <h2><?php _e("Préparation", "agrilogique");?></h2>
            <ol>
            <?php
            if( have_rows('preparation') ):
                while ( have_rows('preparation') ) : the_row();
                    echo '<li>';
                    the_sub_field('step');
                    echo '</li>';
                endwhile;
            endif;
            ?>
            </ol>
        </div>
    </div>
</div>
<?php $terms = wp_get_post_terms( $post->ID, "range" ); ?>
<?php if($term = $terms[0]):?>
    <section>
    <?php 
        $image = get_field('bg-image', $term);
        $bg = '';
        $size = 'bg-image';
        if( $image ) :
            if($bg = wp_get_attachment_image_src( $image, $size )){
                $bg = $bg[0];
            };
        endif;
        ?>
        <div class="pdt-lie" style="background-image:url(<?php echo $bg;?>);">
            <div class="wrapper">
                <div class="fullGrid">
                    <div class="grid30">
                    <?php 
                        $image = get_field('image', $term);
                        $size = 'gamme';
                        if( $image ) :
                            echo wp_get_attachment_image( $image, $size );
                        endif;
                    ?>
                    </div>
                    <div class="grid70">
                    <span><?php _e("Recette fièrement cuisinée avec", "agrilogique");?></span>
                    <?php echo '<p>'.$term->name . '</p>';?>

                    <?php if(!empty(get_field("link", $term)) && $label = get_field("link-label", $term)):?>
                        
                        <a class="button" href="<?php the_field("link", $term);?>" target="_blank"> 
                            <?php echo $label;?>
                        </a>
                    <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>