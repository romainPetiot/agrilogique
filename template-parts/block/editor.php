<?php
/**
 * Block Name: editor
 */
?>
<section class="bloc editor">
    <div class="wrapper">
        <?php the_field("content");?>
    </div>
</section>
