<?php
/**
 * Block Name: proof-slide
 */
?>

<?php 
$image = get_field('bg-image');
$bg = '';
$size = 'bg-image';
if( $image ) :
    if($bg = wp_get_attachment_image_src( $image, $size )){
        $bg = $bg[0];
    };
endif;

$id = uniqid();
?>
<!-- ID sanitize_file_name sert d'ancre. Le fil de fer est un menu WordPress avec des liens personnalisés -->
<section class="bloc proof-slide" id="<?php echo strtolower(sanitize_file_name(get_field("title")));?>" style="background-image:url(<?php echo $bg;?>);">
    <div class="fullGrid"> 
        <div class="grid70">
            <?php
                echo ihag_menu('proof');
            ?>
            <div class="bloc-content">
                <span class="proof-number"><?php _e("Preuve n°", "agrilogique");?><?php the_field("number");?></span>
                <h2>
                    <?php the_field("title");?>
                </h2>
                <?php if(!empty(get_field("video-url")) && $image = get_field('video-img')): ?>
                    <div class="video video-small" data-video="<?php echo htmlentities(get_field("video-url"));?>" data-id="modal-<?php echo $id;?>">
                    <span class="play"></span>
                        <?php
                            $size = 'video-'.get_field("video-size");
                            echo wp_get_attachment_image( $image, $size );
                        ?>
                    </div>
                    <div class="videoIE">
                        <?php if(get_field("video-size") == "small"):?>
                        <?php 
                        preg_match('/src="(.+?)"/', get_field("video-url"), $matches);
                        $src = $matches[1];
                        ?>
                        <div class="video-<?php the_field("video-size");?>"> <!-- video-size return small or big -->
                            <a href="<?php echo $src;?>" target="_blank">
                            <?php
                                $size = 'video-'.get_field("video-size");
                                echo wp_get_attachment_image( $image, $size );
                            ?>
                            </a>
                        </div>
                        <?php
                            else:
                                echo the_field("video-url");
                            endif;
                        ?>
                    </div>
                <?php elseif($image = get_field('video-img')):?>
        
                    <div class="video-small"> <!-- video-size return small or big -->
                        <?php
                            $size = 'video-'.get_field("video-size");
                            echo wp_get_attachment_image( $image, $size );
                        ?>
                    </div>
                
                <?php endif;?>
                <?php the_field("content");?>
            </div>
        </div>
    </div>    
    <div class="modal" id="modal-<?php echo $id;?>"></div>
</section>
