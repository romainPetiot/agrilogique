<?php
/**
 * Block Name: map-farmer
 */
?>
<section class="bloc map-farmer">
    <div class="wrapper">
        <h2><?php the_field("title");?></h2>
        <div class="bg">
            <?php the_field("content");?>
            <img src="<?php the_field('map-svg');?>" alt="carte de la France avec la géolocalisation des agriculteurs">
        </div>
    </div>
</section>
