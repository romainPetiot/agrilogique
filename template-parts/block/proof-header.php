<?php
/**
 * Block Name: proof-header
 */
?>
    
<section class="bloc proof-header">
    <div class="wrapper fullGrid">
    <?php
        $nbProof = 1;
        if( have_rows('proofs') ):
            while ( have_rows('proofs') ) : the_row();
                $image = get_sub_field('picto');
                $size = 'picto-proof';
                if( $image ) :
                    ?>
                        <div class="grid20">
                            <a href="<?php the_sub_field('ancre')?>">
                                <div class="round">
                                <?php
                                    echo wp_get_attachment_image( $image, $size );
                                ?>
                                </div>
                                <div class="number">
                                  <?php echo $nbProof++;?>
                                </div>
                                <span class="title"><?php the_sub_field("title");?></span>
                                <p><?php the_sub_field("baseline");?></p>
                                <div class="more"><span><?php _e("En savoir plus", "agrilogique");?></span><i></i></div>
                            </a>
                        </div>
                    <?php
                endif;
            endwhile;
        endif;
    ?>
    </div>
    
</section>
