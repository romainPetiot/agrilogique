<?php
/**
 * Block Name: slide-video
 */
?>
<?php 
$image = get_field('bg-image');
$bg = '';
$size = 'bg-image';
if( $image ) :
    if($bg = wp_get_attachment_image_src( $image, $size )){
        $bg = $bg[0];
    };
endif;

$id = uniqid();

?>
<section class="bloc slide-video" style="background-image:url(<?php echo $bg;?>);">
    
    <?php if(get_field('video')):?>
        <div class="video" data-video="<?php echo htmlentities(get_field("video"));?>" data-id="modal-<?php echo $id;?>">
            <span class="play"></span>
            <?php 
            $image = get_field('video-img');
            $size = 'large';
            if( $image ) :
                echo wp_get_attachment_image( $image, $size );
            endif;
            ?>

        </div>
        <div class="videoIE">
            <?php the_field("video");?>
        </div>
    <?php elseif($image = get_field('video-img')):?>
        
        <div class=""> <!-- video-size return small or big -->
            <?php
                $size = 'large';
                echo wp_get_attachment_image( $image, $size );
            ?>
        </div>
    
    <?php endif;?>

    <?php
        if(get_field('scroll-down')):
    ?>
    <p>
        <a href="#slide-main" class="ancre-home"><?php _e("ça vous plaît ?
        on vous en dit plus ici", "agrilogique");?>
            <svg width="52px" height="16px" viewBox="0 0 52 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="1-HP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                    <g id="forme" transform="translate(-574.000000, -761.000000)" fill-rule="nonzero" stroke="#FFFFFF">
                        <g id="ecran0" transform="translate(0.000000, -24.000000)">
                            <g id="btn-bas" transform="translate(442.000000, 719.000000)">
                                <polyline id="Path-5" points="132 67 158 81 184 67"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </a>
    </p>
    <?php
        endif;
    ?>
</section>
<div class="modal" id="modal-<?php echo $id;?>">

</div>