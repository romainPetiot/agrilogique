<?php
/**
 * Block Name: slider
 * 
 * 
 * sustainable slider : @src: https://www.w3schools.com/howto/howto_js_slideshow.asp
 * 
 */
if($posts = get_field('farmers')):
?>
<section class="bloc slider">
    <div class="wrapper">
    <script>var farmers = [];</script>
    <?php
    $i=0;
    $posts = get_field('farmers');

    if( $posts ):
       $i = 1; ?>

       <div class="mySlides">
       <?php foreach( $posts as $p): $id = uniqid(); ?>
         
        <div class="bloc-img" data-id="modal-<?php echo $id;?>" data-image="<?php echo get_the_post_thumbnail_url( $p->ID, 'full');?>" data-content="<?php echo strip_tags(get_the_content(0,0,$p->ID));?>">
            <?php echo get_the_post_thumbnail( $p->ID, 'farmer');?>
            <span><?php echo get_the_title($p->ID); ?></span>
            <div class="modal" id="modal-<?php echo $id;?>"></div>
        </div>


        <?php if ( wp_is_mobile() ) : ?>
            <?php if($i % 1 == 0) { ?>
                </div><div class="mySlides">
            <?php } ?>
        
        <?php else : ?>
        
            <?php if($i % 3 == 0) { ?>
                </div><div class="mySlides">
            <?php } ?>
            


        <?php endif; ?>


       <?php $i++; endforeach;echo '</div>'; ?>

       <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
       <a class="next" onclick="plusSlides(1)">&#10095;</a>

   <?php endif; ?>
   </div>
</section>
<?php endif;?>