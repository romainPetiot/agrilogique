<?php
/**
 * Block Name: slide-second-home
 */
?>
<?php 
$image = get_field('bg-image');
$bg = '';
$size = 'bg-image';
if( $image ) :
    if($bg = wp_get_attachment_image_src( $image, $size )){
        $bg = $bg[0];
    };
endif;

$id = uniqid();
?>
<!-- ID sanitize_file_name sert d'ancre. Le fil de fer est un menu WordPress avec des liens personnalisés -->
<section class="bloc slide-second-home" id="<?php echo strtolower(sanitize_file_name(str_replace(array(' ', '<br/>'), '',get_field("title"))));?>" style="background-image:url(<?php echo $bg;?>);">
   <div class="fullGrid"> 
        <div class="grid70">
        <?php
            echo ihag_menu('home');
        ?>
            <div class="bloc-content">
            <?php 
            $image = get_field('picto');
            $size = 'full';
            if( $image ) :
                echo wp_get_attachment_image( $image, $size );
            endif;
            ?>
            <h2>
                <?php the_field("title");?>
            </h2>
            <?php if(!empty(get_field("video-url")) && $image = get_field('video-img')):?>
            <div class="video video-<?php the_field("video-size");?>" data-video="<?php echo htmlentities(get_field("video-url"));?>" data-id="modal-<?php echo $id;?>">
                <div class="video-<?php the_field("video-size");?>"> <!-- video-size return small or big -->
                <span class="play"></span>
                    <?php
                        $size = 'video-'.get_field("video-size");
                        echo wp_get_attachment_image( $image, $size );
                    ?>
                </div>
            </div>
            <div class="videoIE">
                <?php if(get_field("video-size") == "small"):?>
                <?php 
                preg_match('/src="(.+?)"/', get_field("video-url"), $matches);
                $src = $matches[1];
                ?>
                <div class="video-<?php the_field("video-size");?>"> <!-- video-size return small or big -->
                <a href="<?php echo $src;?>" target="_blank">
                <?php
                    $size = 'video-'.get_field("video-size");
                    echo wp_get_attachment_image( $image, $size );
                ?>
                <span class="play"></span>
                </a>
                    
                </div>
                <?php
                    else:
                        echo the_field("video-url");
                    endif;
                ?>
            </div>
            <?php elseif($image = get_field('video-img')):?>
            
                <div class="video-<?php the_field("video-size");?>"> <!-- video-size return small or big -->
                    <?php
                        $size = 'video-'.get_field("video-size");
                        echo wp_get_attachment_image( $image, $size );
                    ?>
                </div>
            
            <?php endif;?>
            <?php the_field("content");?>
            <?php if(!empty(get_field("button-label")) && !empty(get_field("button-link"))):?>
                <a href="<?php the_field("button-link");?>" class="button">
                    <?php the_field("button-label");?>
                </a>
            <?php endif;?>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="modal-<?php echo $id;?>"></div>