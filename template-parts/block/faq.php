<?php
/**
 * Block Name: faq
 */
?>
<section class="bloc faq">
    <div class="accordion-wrapper">
        <button class="accordion"><?php the_field('question'); ?></button>
        <div class="accordion-content">
            <?php the_field("response");?>
        </div>
    </div>
</section>

