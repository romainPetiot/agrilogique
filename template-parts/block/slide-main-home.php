<?php
/**
 * Block Name: slide-main-home
 */
?>
<?php 
$image = get_field('bg-image');
$bg = '';
$size = 'bg-image';
if( $image ) :
    if($bg = wp_get_attachment_image_src( $image, $size )){
        $bg = $bg[0];
    };
endif;
?>
<section class="bloc slide-main-home" style="background-image:url(<?php echo $bg;?>);" id="slide-main">
    <div class="wrapper">
        <?php if(pll_current_language() == 'de'):?>
            <div class="fullGrid  slide-main-home-de">
            <a href="#<?php the_field("ancre1");?>" class="ancre1">
             <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/coop-de.svg" />
            </a>
            <a href="#<?php the_field("ancre2");?>" class="ancre2">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/respect-de.svg" />  
            </a>
            <div class="demarche">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/demarche-de.svg" />
            </div>

            <a href="#<?php the_field("ancre3");?>" class="ancre3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/frais-de.svg" />
                </a>
            <a href="#<?php the_field("ancre4");?>" class="ancre4">    
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/controle-de.svg" />
            </a>
        </div> 
        <?php else:?>
        <div class="fullGrid">
            <a href="#<?php the_field("ancre1");?>" class="ancre1">
             <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/coop.svg" />
            </a>
            <a href="#<?php the_field("ancre2");?>" class="ancre2">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/respect.svg" />  
            </a>
            <div class="demarche">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/demarche.svg" />
            </div>

            <a href="#<?php the_field("ancre3");?>" class="ancre3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/frais.svg" />
                </a>
            <a href="#<?php the_field("ancre4");?>" class="ancre4">    
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/controle.svg" />
            </a>
        </div> 
        <?php endif;?>
        <a href="#notremodelecooperatif" class="ancre-home"><?php _e("ça vous plaît ?
        on vous en dit plus ici", "agrilogique");?>
            <svg width="52px" height="16px" viewBox="0 0 52 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="1-HP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                    <g id="Desktop//HP-v3" transform="translate(-574.000000, -761.000000)" fill-rule="nonzero" stroke="#FFFFFF">
                        <g id="ecran0" transform="translate(0.000000, -24.000000)">
                            <g id="btn-bas" transform="translate(442.000000, 719.000000)">
                                <polyline id="Path-5" points="132 67 158 81 184 67"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </a>  
    </div>
</section>
